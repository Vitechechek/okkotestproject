//
//  AppDelegate.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 25.03.2022.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        self.window = UIWindow(frame: UIScreen.main.bounds)
        self.window?.rootViewController = MoviesCollectionController()
        self.window?.makeKeyAndVisible()

        return true
    }
}

