//
//  DataLoadable.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 31.03.2022.
//

import Foundation

protocol DataLoadable {
    var dataLoadingDataTask: URLSessionDataTask? { get set }
}
