//
//  NetworkManager.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 29.03.2022.
//

import Foundation

class NetworkManager {

    enum NetError: Error {
        case parsing
        case response(Int)
    }

    enum RequestSignature {
        case get(reqModel: Encodable? = nil)
        case post(reqModel: Encodable? = nil)

        var method: String {
            switch self {
            case .get:
                return "GET"
            case .post:
                return "POST"
            }
        }
    }

    func request<T: Decodable>(urlPath: String, reqSignature: RequestSignature, completion: @escaping (Result<T, Error>) -> Void) {

        let completionOnMain: (Result<T, Error>) -> Void = { result in
            DispatchQueue.main.async {
                completion(result)
            }
        }

        var params: [String: Any] = [:]
        switch reqSignature {
        case .get(let reqModel):
            if let dict = reqModel?.dictionary {
                params = dict
            }
        case .post(let reqModel):
            if let dict = reqModel?.dictionary {
                params = dict
            }
        }

        var components = URLComponents()
        components.scheme = "https"
        components.host = "dummyjson.com"
        components.path = urlPath

        if case .get = reqSignature {
            components.queryItems = params.map({ URLQueryItem(name: $0, value: "\($1)") })
        }

        guard let url = components.url else { return }

        var request = URLRequest(url: url)
        request.httpMethod = reqSignature.method
        if case .post = reqSignature {
            request.httpBody = try? JSONSerialization.data(withJSONObject: params, options: [])
        }

        let urlSession = URLSession.shared.dataTask(with: request) { data, response, error in
            if let error = error {
                completionOnMain(.failure(error))
                return
            }

            guard let urlResponse = response as? HTTPURLResponse else { return completionOnMain(.failure(NetError.parsing)) }
            if !(200..<300).contains(urlResponse.statusCode) {
                return completionOnMain(.failure(NetError.response(urlResponse.statusCode)))
            }

            guard let data = data else { return }
            do {
                let users = try JSONDecoder().decode(T.self, from: data)
                completionOnMain(.success(users))
            } catch {
                debugPrint("Could not translate the data to the requested type. Reason: \(error.localizedDescription)")
                completionOnMain(.failure(error))
            }
        }

        urlSession.resume()
    }
}
