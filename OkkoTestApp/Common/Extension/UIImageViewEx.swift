//
//  UIImageViewEx.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 29.03.2022.
//

import UIKit

extension UIImageView {
    @discardableResult
    func downloaded(from url: URL) -> URLSessionDataTask  {
        let dataTask: URLSessionDataTask = URLSession.shared.dataTask(with: url) { data, response, error in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { [weak self] in
                self?.image = image
            }
        }
        dataTask.resume()
        return dataTask
    }

    @discardableResult
    func downloaded(from link: String) -> URLSessionDataTask? {
        guard let url = URL(string: link) else { return nil }
        return downloaded(from: url)
    }
}
