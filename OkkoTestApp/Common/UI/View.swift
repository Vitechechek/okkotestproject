//
//  View.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 07.04.2022.
//

import UIKit

class View: UIView {

    // MARK: - initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.initView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initView() {
        self.translatesAutoresizingMaskIntoConstraints = false
    }

    // MARK: - constraints

    override func updateConstraints() {
        self.constraints()
        super.updateConstraints()
    }

    func constraints() { }
}
