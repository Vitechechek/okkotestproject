//
//  CollectionReusableView.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 07.04.2022.
//

import UIKit

class CollectionReusableView: UICollectionReusableView {

    static var identifier: String {
        Self.description()
    }

    static var kind: String {
        "\(Self.description())Kind"
    }

    // MARK: - initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.initView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initView() { }

    // MARK: - constraints

    override func updateConstraints() {
        self.constraints()
        super.updateConstraints()
    }

    func constraints() { }
}
