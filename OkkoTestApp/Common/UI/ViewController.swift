//
//  ViewController.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 25.03.2022.
//

import UIKit

class ViewController: UIViewController {

    // MARK: - life cycle

    override func viewDidLoad() {
        super.viewDidLoad()

        self.initController()
    }

    // MARK: - initialization

    func initController() {}
}

