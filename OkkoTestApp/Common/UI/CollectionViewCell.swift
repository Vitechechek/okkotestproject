//
//  CollectionViewCell.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 28.03.2022.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {

    static var identifier: String {
        Self.description()
    }

    // MARK: - initialization

    override init(frame: CGRect) {
        super.init(frame: frame)

        self.initView()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func initView() { }

    // MARK: - constraints

    override func updateConstraints() {
        self.constraints()
        super.updateConstraints()
    }

    func constraints() { }
}
