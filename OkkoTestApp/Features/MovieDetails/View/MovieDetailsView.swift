//
//  MovieDetailsView.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 07.04.2022.
//

import UIKit

class MovieDetailsView: View {

    // MARK: - gui variables

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "imagePlaceholder")
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    private lazy var rateLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    private lazy var descriptionLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    // MARK: - initialization

    override func initView() {
        super.initView()

        self.addSubview(self.imageView)
        self.addSubview(self.titleLabel)
        self.addSubview(self.rateLabel)
        self.addSubview(self.descriptionLabel)
    }

    // MARK: - constraints

    override func constraints() {
        super.constraints()

        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.topAnchor),
            self.imageView.leftAnchor.constraint(equalTo: self.leftAnchor),
            self.imageView.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.imageView.heightAnchor.constraint(equalToConstant: 300),

            self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            self.titleLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 16),
            self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),

            self.rateLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            self.rateLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 8),
            self.rateLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),

            self.descriptionLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 8),
            self.descriptionLabel.topAnchor.constraint(equalTo: self.rateLabel.bottomAnchor, constant: 8),
            self.descriptionLabel.rightAnchor.constraint(equalTo: self.rightAnchor, constant: 8),
            self.descriptionLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.bottomAnchor, constant: 16)
        ])
    }

    // MARK: - setter

    func set(model: ItemsResponseModel.Item) {
        self.imageView.downloaded(from: model.thumbnail)
        self.titleLabel.text = model.title
        self.rateLabel.text = "Rating: \(model.rating.description)"
        self.descriptionLabel.text = "Description text here ..."
    }
}
