//
//  MovieDetailsController.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 07.04.2022.
//

import UIKit

class MovieDetailsController: ViewController {

    private lazy var contentView = MovieDetailsView()

    // MARK: - initialization

    override func initController() {
        super.initController()

        self.view.backgroundColor = #colorLiteral(red: 0.4336019158, green: 0.2760822773, blue: 0.7588753104, alpha: 1)
        self.makeConstraints()
    }

    // MARK: - constraints

    private func makeConstraints() {
        self.view.addSubview(self.contentView)

        NSLayoutConstraint.activate([
            self.contentView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.contentView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.contentView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.contentView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    // MARK: - setter

    func set(model: ItemsResponseModel.Item) {
        self.contentView.set(model: model)
    }
}
