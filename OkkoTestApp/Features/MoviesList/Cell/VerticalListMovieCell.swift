//
//  VerticalListMovieCell.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 29.03.2022.
//

import UIKit

class VerticalListMovieCell: CollectionViewCell, DataLoadable {

    // MARK: - variables

    weak var dataLoadingDataTask: URLSessionDataTask?

    // MARK: - gui variables

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "imagePlaceholder")
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    private lazy var rateLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    // MARK: - initialization

    override func initView() {
        super.initView()
        self.contentView.backgroundColor = #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 0.9007398593)
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 15
        self.contentView.addSubview(self.imageView)
        self.contentView.addSubview(self.titleLabel)
        self.contentView.addSubview(self.rateLabel)
    }

    // MARK: - constraints

    override func constraints() {
        super.constraints()

        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.imageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor),
            self.imageView.widthAnchor.constraint(equalToConstant: 160),

            self.titleLabel.leftAnchor.constraint(equalTo: self.imageView.rightAnchor, constant: 8),
            self.titleLabel.topAnchor.constraint(equalTo: self.contentView.topAnchor, constant: 16),
            self.titleLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 8),

            self.rateLabel.leftAnchor.constraint(equalTo: self.imageView.rightAnchor, constant: 8),
            self.rateLabel.topAnchor.constraint(equalTo: self.titleLabel.bottomAnchor, constant: 8),
            self.rateLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 8),
            self.rateLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor, constant: 16)
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.imageView.image = UIImage(named: "imagePlaceholder")
        self.titleLabel.text = ""
    }

    // MARK: - setter

    func set(model: ItemsResponseModel.Item) {
        self.titleLabel.text = model.title
        self.rateLabel.text = "Rating: \(model.rating.description)"
        self.dataLoadingDataTask = self.imageView.downloaded(from: model.thumbnail)

        self.setNeedsUpdateConstraints()
    }
}
