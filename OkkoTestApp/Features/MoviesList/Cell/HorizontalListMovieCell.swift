//
//  HorizontalListMovieCell.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 31.03.2022.
//

import UIKit

class HorizontalListMovieCell: CollectionViewCell, DataLoadable {

    // MARK: - variables

    weak var dataLoadingDataTask: URLSessionDataTask?

    // MARK: - gui variables

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "imagePlaceholder")
        return view
    }()

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.textColor = #colorLiteral(red: 0, green: 0, blue: 0, alpha: 1)
        view.translatesAutoresizingMaskIntoConstraints = false
        view.numberOfLines = 0
        return view
    }()

    // MARK: - initialization

    override func initView() {
        super.initView()
        self.contentView.backgroundColor = #colorLiteral(red: 0.9568627477, green: 0.6588235497, blue: 0.5450980663, alpha: 0.902214404)
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 15
        self.contentView.addSubview(self.imageView)
        self.contentView.addSubview(self.titleLabel)
    }

    // MARK: - constraints

    override func constraints() {
        super.constraints()

        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.imageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            self.imageView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
            self.imageView.heightAnchor.constraint(equalToConstant: 90),

            self.titleLabel.topAnchor.constraint(equalTo: self.imageView.bottomAnchor, constant: 16),
            self.titleLabel.leftAnchor.constraint(equalTo: self.contentView.leftAnchor, constant: 8),
            self.titleLabel.rightAnchor.constraint(equalTo: self.contentView.rightAnchor, constant: 8),
            self.titleLabel.bottomAnchor.constraint(lessThanOrEqualTo: self.contentView.bottomAnchor, constant: 16)
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.imageView.image = UIImage(named: "imagePlaceholder")
        self.titleLabel.text = ""
    }

    // MARK: - setter

    func set(model: ItemsResponseModel.Item) {
        self.titleLabel.text = model.title
        self.dataLoadingDataTask = self.imageView.downloaded(from: model.thumbnail)

        self.setNeedsUpdateConstraints()
    }
}
