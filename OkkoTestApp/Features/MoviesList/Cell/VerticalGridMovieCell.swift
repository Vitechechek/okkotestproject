//
//  VerticalGridMovieCell.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 31.03.2022.
//

import UIKit

class VerticalGridMovieCell: CollectionViewCell, DataLoadable {

    // MARK: - variables

    weak var dataLoadingDataTask: URLSessionDataTask?

    // MARK: - gui variables

    private lazy var imageView: UIImageView = {
        let view = UIImageView()
        view.contentMode = .scaleAspectFill
        view.clipsToBounds = true
        view.translatesAutoresizingMaskIntoConstraints = false
        view.image = UIImage(named: "imagePlaceholder")
        return view
    }()

    // MARK: - initialization

    override func initView() {
        super.initView()
        self.contentView.backgroundColor = #colorLiteral(red: 0.8037419319, green: 0.8037419319, blue: 0.8037419319, alpha: 0.83)
        self.contentView.layer.masksToBounds = true
        self.contentView.layer.cornerRadius = 15
        self.contentView.addSubview(self.imageView)
    }

    // MARK: - constraints

    override func constraints() {
        super.constraints()

        NSLayoutConstraint.activate([
            self.imageView.topAnchor.constraint(equalTo: self.contentView.topAnchor),
            self.imageView.leftAnchor.constraint(equalTo: self.contentView.leftAnchor),
            self.imageView.rightAnchor.constraint(equalTo: self.contentView.rightAnchor),
            self.imageView.bottomAnchor.constraint(equalTo: self.contentView.bottomAnchor)
        ])
    }

    override func prepareForReuse() {
        super.prepareForReuse()

        self.imageView.image = UIImage(named: "imagePlaceholder")
    }

    // MARK: - setter

    func set(model: ItemsResponseModel.Item) {
        self.dataLoadingDataTask = self.imageView.downloaded(from: model.thumbnail)

        self.setNeedsUpdateConstraints()
    }
}
