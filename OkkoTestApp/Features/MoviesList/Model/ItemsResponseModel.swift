//
//  ItemsResponseModel.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 29.03.2022.
//

import Foundation

struct ItemsResponseModel: Decodable {
    struct Item: Decodable {
        let title: String
        let rating: Double
        let category: String
        let thumbnail: String
    }

    let products: [Item]
}
