//
//  MoviesCollectionController.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 28.03.2022.
//

import UIKit

class MoviesCollectionController: ViewController {

    enum SectionType: Int, CaseIterable {
        case verticalList
        case horizontalList
        case verticalGrid

        static func sectionTypeFor(section: Int) -> Self? {
            let sectionIndexStyle = section % SectionType.allCases.count
            return .init(rawValue: sectionIndexStyle)
        }
    }

    // MARK: - variables

    private var models: [String: [ItemsResponseModel.Item]] = [:] {
        didSet {
            self.collectionView.reloadData()
        }
    }

    // MARK: - gui variables

    private lazy var collectionView: UICollectionView = {
        let view = UICollectionView(frame: .zero, collectionViewLayout: self.createLayout())
        view.translatesAutoresizingMaskIntoConstraints = false
        view.showsVerticalScrollIndicator = false
        view.dataSource = self
        view.backgroundColor = .clear
        view.delegate = self
        view.register(VerticalGridMovieCell.self,
                      forCellWithReuseIdentifier: VerticalGridMovieCell.identifier)
        view.register(VerticalListMovieCell.self,
                      forCellWithReuseIdentifier: VerticalListMovieCell.identifier)
        view.register(HorizontalListMovieCell.self,
                      forCellWithReuseIdentifier: HorizontalListMovieCell.identifier)
        view.register(MovieReusableCollectionHeaderView.self,
                      forSupplementaryViewOfKind: MovieReusableCollectionHeaderView.kind,
                      withReuseIdentifier: MovieReusableCollectionHeaderView.identifier)
        return view
    }()

    // MARK: - initialization

    override func initController() {
        super.initController()

        self.view.backgroundColor = #colorLiteral(red: 0.5568627715, green: 0.3529411852, blue: 0.9686274529, alpha: 0.45)
        self.makeConstraints()
        self.requestItems()
    }

    // MARK: - constraints

    private func makeConstraints() {
        self.view.addSubview(self.collectionView)

        NSLayoutConstraint.activate([
            self.collectionView.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor),
            self.collectionView.leftAnchor.constraint(equalTo: self.view.leftAnchor),
            self.collectionView.rightAnchor.constraint(equalTo: self.view.rightAnchor),
            self.collectionView.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor)
        ])
    }

    // MARK: - methods

    private func createLayout() -> UICollectionViewLayout {
        let layout = UICollectionViewCompositionalLayout { (sectionIndex, _) -> NSCollectionLayoutSection? in

            let sectionHeaderSize = NSCollectionLayoutSize(
                widthDimension: .fractionalWidth(1),
                heightDimension: .absolute(50))
            let sectionHeader = NSCollectionLayoutBoundarySupplementaryItem(
                layoutSize: sectionHeaderSize,
                elementKind: MovieReusableCollectionHeaderView.kind,
                alignment: .topLeading)

            guard let sectionType = SectionType.sectionTypeFor(section: sectionIndex) else { return nil }
            switch sectionType {
            case .verticalList:
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalHeight(1),
                    heightDimension: .fractionalHeight(1))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                item.contentInsets = .init(top: 4, leading: 4, bottom: 4, trailing: 4)

                let groupSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .absolute(150))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 1)

                let section = NSCollectionLayoutSection(group: group)
                section.boundarySupplementaryItems = [sectionHeader]
                return section

            case .horizontalList:
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1.0),
                    heightDimension: .fractionalHeight(1.0))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                item.contentInsets = .init(top: 4, leading: 4, bottom: 4, trailing: 4)

                let groupSize = NSCollectionLayoutSize(
                    widthDimension: .absolute(140),
                    heightDimension: .estimated(200))
                let group = NSCollectionLayoutGroup.vertical(layoutSize: groupSize, subitem: item, count: 1)

                let section = NSCollectionLayoutSection(group: group)
                section.boundarySupplementaryItems = [sectionHeader]
                section.orthogonalScrollingBehavior = .continuous
                return section

            case .verticalGrid:
                let itemSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(0.5),
                    heightDimension: .fractionalHeight(1))
                let item = NSCollectionLayoutItem(layoutSize: itemSize)
                item.contentInsets = .init(top: 4, leading: 4, bottom: 4, trailing: 4)

                let groupSize = NSCollectionLayoutSize(
                    widthDimension: .fractionalWidth(1),
                    heightDimension: .absolute(175))
                let group = NSCollectionLayoutGroup.horizontal(layoutSize: groupSize, subitem: item, count: 2)

                let section = NSCollectionLayoutSection(group: group)
                section.boundarySupplementaryItems = [sectionHeader]
                return section
            }
        }

        return layout
    }

    private func openDetails(with model: ItemsResponseModel.Item) {
        let vc = MovieDetailsController()
        vc.set(model: model)
        self.present(vc, animated: true)
    }

    // MARK: - request

    private func requestItems() {
        NetworkManager().request(urlPath: "/products", reqSignature: .get()) { [weak self] (result: Result<ItemsResponseModel, Error>) in
            guard let self = self else { return }
            switch result {
            case .success(let model):
                self.models = Dictionary(grouping: model.products, by: { $0.category })
            case .failure(let error):
                print(error)
            }
        }
    }
}

extension MoviesCollectionController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return Array(self.models.keys).count
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let key = Array(self.models.keys)[safe: section],
                let items = self.models[key] else { return 0 }
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let sectionType = SectionType.sectionTypeFor(section: indexPath.section),
                let key = Array(self.models.keys)[safe: indexPath.section],
                let model = models[key]?[safe: indexPath.row] else { return UICollectionViewCell() }
        switch sectionType {
        case .verticalList:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VerticalListMovieCell.identifier, for: indexPath)
                    as? VerticalListMovieCell else { return UICollectionViewCell() }
            cell.set(model: model)
            return cell

        case .horizontalList:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HorizontalListMovieCell.identifier, for: indexPath)
                    as? HorizontalListMovieCell else { return UICollectionViewCell() }
            cell.set(model: model)
            return cell
            
        case .verticalGrid:
            guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: VerticalGridMovieCell.identifier, for: indexPath)
                    as? VerticalGridMovieCell else { return UICollectionViewCell() }
            cell.set(model: model)
            return cell
        }
    }
}

extension MoviesCollectionController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        guard let expiredCell = cell as? DataLoadable else { return }
        expiredCell.dataLoadingDataTask?.cancel()
    }

    func collectionView(_ collectionView: UICollectionView, viewForSupplementaryElementOfKind kind: String, at indexPath: IndexPath) -> UICollectionReusableView {
        guard let header = collectionView.dequeueReusableSupplementaryView(ofKind: kind, withReuseIdentifier: MovieReusableCollectionHeaderView.identifier, for: indexPath)
                as? MovieReusableCollectionHeaderView, let key = Array(self.models.keys)[safe: indexPath.section] else { return UICollectionReusableView() }
        header.set(title: key.description)
        return header
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let key = Array(self.models.keys)[safe: indexPath.section],
                let model = models[key]?[safe: indexPath.row] else { return }
        self.openDetails(with: model)
    }
}
