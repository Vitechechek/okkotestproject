//
//  MovieReusableCollectionHeaderView.swift
//  OkkoTestApp
//
//  Created by Viktor Vigonyailo on 07.04.2022.
//

import UIKit

class MovieReusableCollectionHeaderView: CollectionReusableView {

    // MARK: - gui variables

    private lazy var titleLabel: UILabel = {
        let view = UILabel()
        view.numberOfLines = 1
        view.translatesAutoresizingMaskIntoConstraints = false
        view.textColor = .white
        return view
    }()

    // MARK: - initialization

    override func initView() {
        super.initView()

        self.addSubview(self.titleLabel)
    }

    // MARK: - constraints

    override func constraints() {
        super.constraints()

        NSLayoutConstraint.activate([
            self.titleLabel.topAnchor.constraint(equalTo: self.topAnchor),
            self.titleLabel.leftAnchor.constraint(equalTo: self.leftAnchor, constant: 20),
            self.titleLabel.rightAnchor.constraint(equalTo: self.rightAnchor),
            self.titleLabel.bottomAnchor.constraint(equalTo: self.bottomAnchor)
        ])
    }

    // MARK: - reuse

    override func prepareForReuse() {
        super.prepareForReuse()

        self.titleLabel.text = nil
    }

    // MARK: - setter

    func set(title: String) {
        self.titleLabel.text = title
    }
}
